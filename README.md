gitlab-rulez
============

Compute and apply changes to the settings of the projects in a GitLab instance
based on a set of YAML rules.

First, configure the GitLab access credentials for python-gitlab:
https://python-gitlab.readthedocs.io/en/stable/cli-usage.html#configuration

Create a `rulez.yaml` file with expected GitLab setting rules, see `sample-rulez.yaml`.

Then check what currently violates the rules so far defined:

    gitlab-rulez diff ./rulez.yaml

If what gitlab-rulez highlights seem sensible, tell it to poke your GitLab
instance until it is happy:

    gitlab-rulez apply ./rulez.yaml

To limit the changes to a single project, use `--filter`:

    gitlab-rulez apply ./rulez.yaml --filter pkg/target/systemd
